import torch
from IPython.display import Image  # for displaying images
import os
import random
import shutil
from sklearn.model_selection import train_test_split
import xml.etree.ElementTree as ET
from xml.dom import minidom
from tqdm import tqdm
from PIL import Image, ImageDraw
import numpy as np
import matplotlib.pyplot as plt

#random.seed(108)

# Training
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
YOLO_DIR = os.path.abspath(os.path.join(ROOT_DIR,"..","yolov5"))
DATA_DIR = os.path.abspath(os.path.join(ROOT_DIR,"..","datasets","berry"))
DATA_IMAGES_DIR = os.path.abspath(os.path.join(DATA_DIR,"images"))
DATA_LABELS_DIR = os.path.abspath(os.path.join(DATA_DIR,"labels"))
DATA_ANNOT_DIR = os.path.abspath(os.path.join(DATA_DIR,"annotations"))

# Generation
GEN_DIR = os.path.abspath(os.path.join(ROOT_DIR,"..","..","generation"))
GEN_LABELS_DIR = os.path.abspath(os.path.join(GEN_DIR,"labels"))
GEN_IMAGES_DIR = os.path.abspath(os.path.join(GEN_DIR,"output"))

paths = [DATA_DIR, DATA_IMAGES_DIR, DATA_LABELS_DIR, DATA_ANNOT_DIR]
print("")
for dir in paths:
    if not os.path.exists(dir):
        print("Creating a directory: " + str(dir))
        os.makedirs(dir)
print("")

def count_files(directory):
    return len([name for name in os.listdir(directory) if os.path.isfile(os.path.join(directory,name))])

def update_training_data():
    if (count_files(GEN_LABELS_DIR) < count_files(DATA_LABELS_DIR)):
        print("Generated directory has less files than training directory. Skipping...")
    elif (count_files(GEN_LABELS_DIR) == count_files(DATA_LABELS_DIR)):
        print("No need to update. Skipping...")
    else:
        print("Copying Labels...")
        shutil.copytree(GEN_LABELS_DIR, DATA_LABELS_DIR, dirs_exist_ok=True)

        print("Copying Images...")
        shutil.copytree(GEN_IMAGES_DIR, DATA_IMAGES_DIR, dirs_exist_ok=True)

        print("Updating training filelist...")
        file = open(os.path.join(DATA_DIR,"berry_train.txt"),"a")
        for i in range(count_files(DATA_LABELS_DIR)):
            file_name = "./images/{}.png".format(i)
            file.write("{}\n".format(file_name))
        file.close()

if __name__ == "__main__":
    # Move generated labels, images and update filelists
    update_training_data()
    shutil.copy(os.path.join(ROOT_DIR,"..","berry.yaml"),YOLO_DIR)

    os.chdir(YOLO_DIR)

    # Load model
    model = torch.hub.load('ultralytics/yolov5', 'yolov5s')

    #Train
    os.system('python train.py --img 512 --batch 16 --epochs 10 --data berry.yaml --weights yolov5s.pt')
