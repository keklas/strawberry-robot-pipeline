#!/bin/bash
eecho() {
	echo -e "================================"
	echo -e $1
	echo -e "================================"
}

git clone https://github.com/ultralytics/yolov5 &&
	cd yolov5 &&
	git reset --hard 886f1c03d839575afecb059accf74296fad395b6 &&
	cd ..

python3 -m pip install --user virtualenv
python3 -m venv env

eecho "Installing YOLOv5 dependencies..."
$(pwd)/env/bin/pip install -r yolov5/requirements.txt
$(pwd)/env/bin/python3 -m pip --version
