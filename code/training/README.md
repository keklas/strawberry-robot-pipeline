# Training
This section covers training a neural network with a YOLOv5 algorithm. Training uses synthetic data generated earlier.

## Installation
- Run the install script in the directory: `./install.sh`. This downloads the YOLO git repo and sets up a virtual python environment
- As of 30.1.2022, python module `torch` does not work on python version 3.10. You need to downgrade python to 3.9.xx
  - In archlinux you can do this with `yay -Sy python39`
  - Delete the env/ directory
  - Try the installation again

## Usage
- To keep your own packages organized, all packages are downloaded in a virtual environment
- Activate the venv with `source env/bin/activate`
- Run the main code with `python3.9 src/main.py

- Deactivate it with, you guessed it: `deactivate`

-
