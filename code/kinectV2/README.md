# KinectV2 testing container

## Usage
- Make sure that your docker daemon is running: `sudo systemctl start docker`
- Make sure that your user belongs to the `docker` group
  - You can check this with the command `groups`
  - If your user does not belong to this group run:

```
sudo groupadd docker
sudo usermod -aG docker ${USER}
```

  - Reboot
- Build the image with `docker build . -t kinectv2`
- Connect kinectV2 to your computer
- Find out where in `/dev` it is located in
- Run the image with `docker run -it kinectv2 --device=/dev/[CHANGE THIS]`

## Debug
I have no idea how this behaves when the USB camera is plugged in. It might fail when trying to draw windows that are spawned within the container.
- If you want to look at the files in the container, you can run it interactively without the USB device
  - Remove the line `CMD ./libfreenect2/build/bin/Protonect` from the Dockerfile
  - Run: `docker run -it kinectv2`
- The container might have to be in privileged mode. Read more about it [here](https://stackoverflow.com/questions/24225647/docker-a-way-to-give-access-to-a-host-usb-or-serial-device)

