#!/usr/bin/python

import blenderproc as bproc
import argparse
import os, h5py, bpy, random, re, math, imagehash, shutil, traceback
import time
import cv2
import numpy as np
import pandas as pd

from scipy.interpolate import interp1d
from PIL import Image as im
from skimage import morphology

RAW_COUNT = 0
RIPE_COUNT = 0
bbox_limit = 30

# Main paths
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
DATA_DIR = os.path.abspath(os.path.join(ROOT_DIR,"..","tmp","output"))
DATA_MERGED_DIR = os.path.abspath(os.path.join(ROOT_DIR,"..","output"))
HDF5_DIR = os.path.abspath(os.path.join(ROOT_DIR,"..","tmp","hdf5"))
CSV_TMP_DIR = os.path.abspath(os.path.join(ROOT_DIR,"..","tmp","csv"))
CSV_DIR = os.path.abspath(os.path.join(ROOT_DIR,"..","csv"))
LABELS_DIR = os.path.abspath(os.path.join(ROOT_DIR,"..","labels"))

# Segmentation image paths
IMG_DIR = os.path.abspath(os.path.join(ROOT_DIR,"..","tmp","step1"))
RIPE_DIR = os.path.abspath(os.path.join(IMG_DIR,"ripe"))
RAW_DIR = os.path.abspath(os.path.join(IMG_DIR,"raw"))

# Segmentation filtering paths
FILTER_DIR = os.path.abspath(os.path.join(ROOT_DIR,"..","tmp","step2"))
RIPE2_DIR = os.path.abspath(os.path.join(FILTER_DIR,"ripe"))
RAW2_DIR = os.path.abspath(os.path.join(FILTER_DIR,"raw"))

# Bounding boxes
BB_DIR = os.path.abspath(os.path.join(ROOT_DIR,"..","tmp","step3"))
RIPE3_DIR = os.path.abspath(os.path.join(BB_DIR,"ripe"))
RAW3_DIR = os.path.abspath(os.path.join(BB_DIR,"raw"))

# CSV files
CSV_RAW = os.path.join(CSV_DIR,"raw.csv")
CSV_RIPE = os.path.join(CSV_DIR,"ripe.csv")
CSV_DATA = os.path.join(CSV_DIR,"data.csv")

# Imports
IMP_DIR = os.path.abspath(os.path.join(ROOT_DIR,"..","import"))
IMPD_DIR = os.path.abspath(os.path.join(ROOT_DIR,"..","imported"))

# Make missing paths
paths = [DATA_DIR, DATA_MERGED_DIR, CSV_DIR, HDF5_DIR, RIPE_DIR, RAW_DIR, RIPE2_DIR, RAW2_DIR, RIPE3_DIR, RAW3_DIR, CSV_TMP_DIR, LABELS_DIR, IMP_DIR, IMPD_DIR]

print("")
for dir in paths:
    if not os.path.exists(dir):
        print("Creating a directory: " + str(dir))
        os.makedirs(dir)
print("")

# Dataframe columns
col_names = ["image_id","image_hash","width","height","bbox","type","depth"]

# Create empty csv files if missing
if not os.path.isfile(CSV_RAW):
    df_source_raw = pd.DataFrame(columns=col_names)
    df_source_raw.to_csv(CSV_RAW, index=False)
if not os.path.isfile(CSV_RIPE):
    df_source_ripe = pd.DataFrame(columns=col_names)
    df_source_ripe.to_csv(CSV_RIPE, index=False)

#==========================================

def count_files(directory):
    return len([name for name in os.listdir(directory) if os.path.isfile(os.path.join(directory,name))])

def load_hdf5_data(i,data):
    directory = os.path.join(HDF5_DIR,"{}.hdf5".format(i))
    with h5py.File(directory,"r+") as file:
        return np.array(file[data])

def generate(amount):
    parser = argparse.ArgumentParser()
    parser.add_argument('scene', nargs='?', default="blender/strawberry.blend", help="Path to the scene.obj file")
    parser.add_argument('output_dir', nargs='?', default=HDF5_DIR, help="Path to where the final files, will be saved")
    args = parser.parse_args()

    bproc.init()

    # load the objects into the scene
    objs = bproc.loader.load_blend(args.scene)

    # define the camera intrinsics
    bproc.camera.set_resolution(512, 512)
    bproc.renderer.set_max_amount_of_samples(400)

    light = bproc.types.Light()
    light.set_type("POINT")
    light.set_location([-0.1, 0.4, 0.1])
    light.set_energy(20)

    # Simulation Randomiztion for each rendered frame
    seed_start = random.randint(-99999,0)
    seed_end = random.randint(amount,99999)

    sim = bpy.data.objects["field"]
    sim.modifiers["GeometryNodes"]["Input_7"] = seed_start
    sim.keyframe_insert(data_path='modifiers["GeometryNodes"]["Input_7"]', frame=0.0)
    sim.modifiers["GeometryNodes"]["Input_7"] = seed_end
    sim.keyframe_insert(data_path='modifiers["GeometryNodes"]["Input_7"]', frame=amount)

    # Sample camera poses
    for i in range(amount):
        location = np.random.uniform([-0.124873, 0, 0.08325], [-0.124873, 0.34, 0.08325])
        rotation_matrix = np.array([
            [0, 0.165, 0.986],
            [1, 0, 0],
            [0, 0.986, -0.165]
        ])
        cam2world_matrix = bproc.math.build_transformation_mat(location, rotation_matrix)
        bproc.camera.add_camera_pose(cam2world_matrix)

    # activate distance rendering
    bproc.renderer.enable_distance_output(activate_antialiasing=False)

    # render the whole pipeline
    data = bproc.renderer.render()
    #dist = data["distance"][0]

    # Render segmentation masks (per class and per instance)
    data.update(bproc.renderer.render_segmap(map_by=["instance"]))

    # Convert distance to depth
    #data["depth"] = bproc.postprocessing.dist2depth(interpolated)
    data["depth"] = bproc.postprocessing.dist2depth(data["distance"])
    del data["distance"]

    # write the data to a .hdf5 container
    bproc.writer.write_hdf5(args.output_dir, data)

def depth_interpolation(n):
    for i in range(n):
        file = "{}.hdf5".format(i)
        directory = os.path.join(HDF5_DIR,file)
        with h5py.File(directory,"r+") as file:
            array = np.array(file["depth"])
            dim = len(array)

            interpolate = interp1d([0.1,0.45],[5,35])

            print("Interpolating depth data {}/{} - {}".format(str(i+1),n,str(file)))
            for i in range(len(array)):
                if array[i][0] == 70:
                    break
                for j in range(len(array[i])):
                    val = array[i][j]
                    if val < 0.45:
                        array[i][j] = float(interpolate(val))
                    else:
                        array[i][j] = 70
            del file["depth"]
            file.create_dataset("depth",data=array,dtype="f")
            file.close()

# Step 1
def segmentation_split(n):
    for i in range(n):
        array = load_hdf5_data(i,"instance_segmaps")
        dim = len(array)

        # replace values outside of the range by 0
        raw = np.where(array<9, 0, array)
        ripe = np.where(array>8, 0 , array)
        ripe = np.where(ripe<7, 0, ripe)

        # convert ripe and raw berries to grayscale
        raw = np.where(raw==9, 100, raw)
        raw = np.where(raw==10, 200, raw)

        ripe = np.where(ripe==7, 100, ripe)
        ripe = np.where(ripe==8, 200, ripe)

        raw_file = "raw_{}.png".format(i)
        ripe_file = "ripe_{}.png".format(i)

        im.fromarray(raw).save(os.path.join(RAW_DIR,raw_file))
        im.fromarray(ripe).save(os.path.join(RIPE_DIR,ripe_file))

def black_to_alpha(img):
    pixdata = img.load()

    width, height = img.size
    for y in range(height):
        for x in range(width):
            if pixdata[x, y] == (0, 0, 0, 255):
                pixdata[x, y] = (0, 0, 0, 0)
    return img

def connect_occluded_berries(array, height_threshold):
    for y in range(len(array)):
        # Exit if the next y + threshold is outside the array
        if ((y + 1 + height_threshold) > len(array)):
            break
        for x in range(len(array[y])):
            # If the current value is true and the next y value is false
            if array[y][x]==True and array[y+1][x]==False:
                # Go through all values up until the threshold
                for yy in range(y+1,y+1+height_threshold):
                    if array[yy][x] == True:
                        # Change all values in between to True
                        for value in range(y+1,yy):
                            array[value][x] = True
                        break
    return array

def segmentation_filtering(n, t):
    form = str(t)
    for i in range(n):
        if(form == "ripe"):
            file = "ripe_{}.png".format(i)
            dir_in = os.path.join(RIPE_DIR,file)
            dir_out = os.path.join(RIPE2_DIR,file)
            print("Filtering ripe berries {}/{} - {}".format(str(i + 1),n,str(file)))
        elif(form == "raw"):
            file = "raw_{}.png".format(i)
            dir_in = os.path.join(RAW_DIR,file)
            dir_out = os.path.join(RAW2_DIR,file)
            print("Filtering raw berries {}/{} - {}".format(str(i + 1),n,str(file)))

        else:
            print("Incorrect option. Use 'ripe' or 'raw'")
            break

        img = im.open(dir_in)
        orig1 = (np.array(img) == 100)
        orig2 = (np.array(img) == 200)

        # Apply erosion and dilation = opens up areas, removes noise
        img1 = morphology.opening(orig1,morphology.square(8))
        img2 = morphology.opening(orig2,morphology.square(8))

        ## Apply dilation and erosion = unifies two nearby areas
        img1 = morphology.closing(img1,morphology.square(10))
        img2 = morphology.closing(img2,morphology.square(10))

        ## Remove small isolated and uniform areas
        img1 = morphology.remove_small_objects(img1,min_size=450,connectivity=1)
        img2 = morphology.remove_small_objects(img2,min_size=450,connectivity=1)

        ## Inflate the image to capture the edges
        img1 = morphology.dilation(img1,morphology.square(8))
        img2 = morphology.dilation(img2,morphology.square(8))

        ## Fill small holes
        img1 = morphology.remove_small_holes(img1,area_threshold=600, connectivity=1)
        img2 = morphology.remove_small_holes(img2,area_threshold=600, connectivity=1)

        ## Connects an occluded berry together
        img1 = connect_occluded_berries(img1, 40)
        img2 = connect_occluded_berries(img2, 40)

        # Convert binary data to RGB image
        img1 = np.where(img1==True, 0.6, img1) # RGB = 153
        img2 = np.where(img2==True, 1, img2)   # RGB = 255
        img1 = im.fromarray((img1 * 255).astype(np.uint8)).convert("RGBA")
        img2 = im.fromarray((img2 * 255).astype(np.uint8)).convert("RGBA")

        # Add alpha layer to all black pixels
        img1_a = black_to_alpha(img1)
        img2_a = black_to_alpha(img2)

        # Merge the two pictures above into one
        combined = im.alpha_composite(img1_a, img2_a).convert("RGB")

        # Save pictures
        combined.save(dir_out)

def export_rendered_images(n):
    for i in range(n):
        file = "{}.png".format(i)
        dir_in = os.path.join(HDF5_DIR,file)
        dir_out = os.path.join(DATA_DIR,file)
        img = im.fromarray(load_hdf5_data(i,"colors")).save(dir_out)
        print("Exporting rendered images {}/{} - {}".format(str(i+1),n, str(file)))

def get_depth_data(i,x,y,w,h):
    data = load_hdf5_data(i,"depth")
    y_coord = y+math.floor(h/2)
    x_coord = x+math.floor(w/2)
    d = data[y_coord][x_coord]
    #print("({}, {}) = {:.2f}". format(x_coord,y_coord,d))
    return d

def get_image_hash(img):
    return imagehash.average_hash(img)

def get_newest_df(t):
    file = "{}.csv".format(str(t))
    try:
        path = os.path.join(CSV_DIR,file)
    except:
       print("Error in reading csv from {}".format(path))
       exit
    return pd.read_csv(os.path.join(CSV_TMP_DIR,file),index_col=False)
    #form = str(t)
    #if(form == "ripe"):
    #    return pd.read_csv(os.path.join(CSV_TMP_DIR,"ripe.csv"))
    #elif(form == "raw"):
    #    return pd.read_csv(os.path.join(CSV_TMP_DIR,"raw.csv"))

def get_source_df(t):
    file = "{}.csv".format(str(t))
    try:
        path = os.path.join(CSV_DIR,file)
    except:
       print("Error in reading csv from {}".format(path))
       exit
    return pd.read_csv(os.path.join(CSV_DIR,file),index_col=False)

    #form = str(t)
    #if(form == "ripe"):
    #    return pd.read_csv(os.path.join(CSV_DIR,"ripe.csv"))
    #elif(form == "raw"):
    #    return pd.read_csv(os.path.join(CSV_DIR,"raw.csv"))

def get_df(t,dir):
    file = "{}.csv".format(str(t))
    try:
        path = os.path.join(dir,file)
    except:
       print("Error in reading csv from {}".format(path))
       exit
    return pd.read_csv(os.path.join(dir,file),index_col=False)

def df_contains_hash(df,image_hash):
    return df["image_hash"].str.contains(str(image_hash)).any()

def df_contains_image_id(df,image_id):
    return image_id in df["image_id"].values

def create_bbox_from_threshold(i,file,form,df,masks,img,dir_out):
    count = 0
    for mask in masks:
        contours = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours = contours[0] if len(contours) == 2 else contours[1]
        count += len(contours)
        for cntr in contours:
            x,y,w,h = cv2.boundingRect(cntr)
            # Add max limit
            if w <= bbox_limit or h <= bbox_limit:
                continue

            # Export images with rectangles (for debugging)
            cv2.rectangle(img, (x, y), (x+w, y+h), (0, 0, 255), 2)

            # Annotations data for each image
            bbox = [x,y,w,h]
            depth = get_depth_data(i,x,y,w,h)
            image_hash = get_image_hash(im.open(os.path.join(DATA_DIR,"{}.png".format(i))))
            if depth >= 50:
                print("EXCLUDING ANNOTATIONS FOR {}, HASH: {}. DEPTH IS MORE THAN 50".format(file,image_hash))
                continue
            dim = len(img)

            # Add to a dataframe
            df = df.append({'image_hash' : str(image_hash),
                            'width' : dim,
                            'height' : dim,
                            'bbox' : bbox,
                            'type' : form,
                            'depth' : depth}, ignore_index=True)
    if count == 0:
        print("Image {} does not have any {} berries".format(file,form))
    cv2.imwrite(dir_out,img)
    return df

def update_annotations(n,t):
    df = pd.DataFrame(columns=col_names)

    form = str(t)
    for i in range(n):
        if(form == "ripe"):
            file = "ripe_{}.png".format(i)
            dir_in = os.path.join(RIPE2_DIR,file)
            dir_out = os.path.join(RIPE3_DIR,file)
            csv_out = os.path.join(CSV_TMP_DIR,"ripe.csv")
        elif(form == "raw"):
            file = "raw_{}.png".format(i)
            dir_in = os.path.join(RAW2_DIR,file)
            dir_out = os.path.join(RAW3_DIR,file)
            csv_out = os.path.join(CSV_TMP_DIR,"raw.csv")
        else:
            print("Incorrect option. Use 'ripe' or 'raw'")
            break
        print("Writing {} annotations {}/{} {}".format(form, str(i+1),n,str(file)))

        # Load image
        img = cv2.imread(dir_in)

        # Convert to grayscale
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

        # Separate by color
        mask1 = cv2.inRange(gray, 100, 200) # gray
        mask2 = cv2.inRange(gray, 200, 255) # white

        # Create labels from contours
        result = img.copy()
        df = create_bbox_from_threshold(i,file,form, df, [mask1,mask2], result, dir_out)
        df.to_csv(csv_out, index=False)

def add_image_id(image_id,image_hash,data):
    df = data[data["image_hash"].str.contains(str(image_hash))]
    df = df.fillna(image_id).astype({"image_id": int})
    return df

def export_csv(n):
    # Get dataframes
    df_new_raw = get_newest_df("raw")
    df_source_raw = get_source_df("raw")
    df_new_ripe = get_newest_df("ripe")
    df_source_ripe = get_source_df("ripe")

    # Create empty df for processing the ones above
    df_raw = pd.DataFrame(columns=col_names).astype({"image_id": int})
    df_ripe = pd.DataFrame(columns=col_names).astype({"image_id": int})

    # Go through all rendered images from the simulation
    filecount = count_files(DATA_MERGED_DIR)
    skipped = 0
    for i in range(n):
        image_id = int(i + filecount - skipped)
        file = "{}.png".format(i)
        dir_in = os.path.join(DATA_DIR,file)
        dir_out = os.path.join(DATA_MERGED_DIR,"{}.png".format(image_id))
        image_hash = get_image_hash(im.open(dir_in))
        print("Appending csv data {}/{}. {}.png -> {}.png, hash: {}".format(str(i+1),n,i,image_id,image_hash))

        # SKIP if the hash of the image is already in the source_raw CSV or source_ripe CSV
        if df_contains_hash(df_source_raw, image_hash) or df_contains_hash(df_source_ripe, image_hash):
            print("  Image {}, hash {} is already in csv, skipping".format(file,image_hash))
            continue
        else:
            print("  Image {}, hash {} is not yet in the csv".format(file,image_hash))

        # New dataframe does not yet contain image_id, add it
        df_raw = pd.concat([df_raw, add_image_id(image_id,image_hash,df_new_raw)])
        df_ripe = pd.concat([df_ripe, add_image_id(image_id,image_hash,df_new_ripe)])

        # Copy images to merged directory ONLY if the TMP df contains the hash of the image AND
        # the SOURCE df does NOT contain the hash
        #
        # if both dataframes have the hash, then the image is already in the the merged directory
        # RAW
        #if (df_contains_hash(df_raw,image_hash)) or (df_contains_hash(df_ripe,image_hash)):
        #    if not (df_contains_hash(df_source_raw,image_hash)) or (df_contains_hash(df_source_ripe,image_hash)):
        #        # Rename the index of the image to merged directory
        #        shutil.copyfile(dir_in,dir_out)
        shutil.copyfile(dir_in,dir_out)
        #        pass
        #else:
        #    print("  Image {}, hash {} is not in the dataset, skipping".format(file,image_hash))
        #    skipped += 1
# 00000000dfdfe400 796.png
    # Merge new and source
    try:
        df_new_raw = df_raw
        df_new_ripe = df_ripe
        raw = pd.concat([df_source_raw, df_raw])
        ripe = pd.concat([df_source_ripe, df_ripe])
        raw.drop_duplicates(subset=['image_hash','bbox','depth']).reset_index(drop=True)
        ripe.drop_duplicates(subset=['image_hash','bbox','depth']).reset_index(drop=True)

        raw.to_csv(os.path.join(CSV_DIR,"raw.csv"),index=False)
        ripe.to_csv(os.path.join(CSV_DIR,"ripe.csv"),index=False)
    except Exception:
        print("An error occured")
        print(traceback.format_exc())

def merge_csv():
    ripe = get_source_df("ripe")
    raw = get_source_df("raw")
    data = pd.concat([ripe, raw]).sort_values(by="image_id")
    data.to_csv(os.path.join(CSV_DIR,"data.csv"),index=False)

def format_to_YOLO(img_width,img_height,location,t):
    index = 0
    if(str(t) == "raw"):
        index += 0
    elif(str(t) == "ripe"):
        index += 1

    location = location.strip("][",).split(", ")
    # Location dimensions
    x = int(location[0])
    y = int(location[1])
    w = int(location[2])
    h = int(location[3])
    #print("x,y,w,h",x,y,w,h)

    # Midpoints
    x_mid = round(x+(w/2))
    y_mid = round(y+(h/2))
    #print("x_mid,y_mid", x_mid,y_mid)

    # YOLO format, normalized
    x_yolo = (x_mid/img_width)
    y_yolo = (y_mid/img_height)
    w_yolo = (w/img_width)
    h_yolo = (h/img_height)

    return [index, x_yolo, y_yolo, w_yolo, h_yolo]

def export_YOLO_labels(n):
    data = get_source_df("data")
    count = max(data["image_id"]) + 1
    print("Writing labels to {} files".format(n))
    for i in range(count):
        file_name = "{}.txt".format(i)
        output = os.path.join(LABELS_DIR,file_name)
        if os.path.isfile(output):
            continue
        else:
            img_data = data.loc[data["image_id"] == i]
            #print("Writing labels to file {}" .format(file_name))
            file = open(output,"a")
            for row in img_data.values:
                img_width = row[2]
                img_height = row[3]
                location = row[4]
                t = row[5]
                values = format_to_YOLO(img_width,img_height,location,t)
                file.write("{} {} {} {} {}\n".format(values[0],
                                                   values[1],
                                                   values[2],
                                                   values[3],
                                                   values[4]))
            file.close()

def import_data():
    if len(os.listdir(IMP_DIR)) == 0:
        print("Nothing to import\n")
    else:
        dirs = os.listdir(IMP_DIR)
        while True:
            print("Which folder should be imported?")
            for dir in dirs:
                print(" {}".format(dir))
            option = str(input())
            if not option in dirs:
                print("Incorrect filename\n")
            else:
                break

        # Check that images and labels exist
        path = os.path.join(IMP_DIR,option)
        try:
            images = os.path.join(path,"output")
            labels = os.path.join(path,"labels")
            csv = os.path.join(path,"csv")
            if(count_files(images) != count_files(labels)):
                print("The amount of images and labels should be the same")
                exit
            try:
                # Images and labels
                files = os.listdir(images)
                filecount = count_files(DATA_MERGED_DIR)
                for file in sorted(files):
                    file_base = int(os.path.splitext(file)[0])
                    file_base_new = (file_base + filecount)
                    img_old = "{}.{}".format(file_base,"png")
                    img_new = "{}.{}".format(file_base_new,"png")
                    lab_old = "{}.{}".format(file_base,"txt")
                    lab_new = "{}.{}".format(file_base_new,"txt")
                    shutil.copy(os.path.join(images,img_old),os.path.join(DATA_MERGED_DIR,img_new))
                    shutil.copy(os.path.join(labels,lab_old),os.path.join(LABELS_DIR,lab_new))

                # csv
                df = get_df("data",csv)
                df_src = get_df("data",CSV_DIR)
                df["image_id"] += filecount

                df_src = pd.concat([df_src, df]).sort_values(by="image_id")
                df_src.drop_duplicates(subset=['image_hash','bbox','depth']).reset_index(drop=True)
                df_src.to_csv(CSV_DATA,index=False)
                shutil.move(path,IMPD_DIR)

            except:
                print("do you even kode")
                print(traceback.format_exc())
        except:
            print("Images or Labels are missing from {}".format(path))





def check_data_validity(n):
    print("========================")
    if (count_files(DATA_MERGED_DIR) < n):
        print(" [INFO] - Less than 1000 images were exported")
    if (count_files(LABELS_DIR) < n):
        print(" [INFO] - Less than 1000 labels were exported")
    if (count_files(LABELS_DIR) != count_files(DATA_MERGED_DIR)):
        print(" [INFO] - The amount of images and labels should be the same")
    print("========================")

if __name__ == "__main__":
    # Amount of files to generate and process
    n = 1000

    import_data()
    # Generate images of strawberries
    #generate(n)
    #export_rendered_images(n)

    # Apply custom adjustments to depth data
    #depth_interpolation(n)

    # Step 1. Separates raw and ripe berries into masks
    #segmentation_split(n)

    # Step 2. Filters out stems and other junk from the mask
    #segmentation_filtering(n, "raw")
    #segmentation_filtering(n, "ripe")

    # Step 3. Write data labels to csv
    #update_annotations(n, "raw")
    #update_annotations(n, "ripe")

    # Merges all images and csv files together avoiding duplicates
    #export_csv(n)
    #merge_csv()

    # Exports labels in YOLO format, needed for training
    export_YOLO_labels(n)

    check_data_validity(n)
